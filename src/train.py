# ==============================================================================
# exercise4.1.I: CNN for CIFAR10 with >70% accuracy
# ==============================================================================

# Usage:
# none

# ==============================================================================
# Constants, Imports
# ==============================================================================
# constants
COMET_EXPERIMENT = True
NOTEBOOK = False
REFERENCES = [
        "https://git.rwth-aachen.de/3pia/vispa/dlipr", # [1]
        "https://github.com/DavidWalz/dlipr", # [2]
    ]
    
CLASS_LABELS = ["airplane", "automobile", "bird", "cat", "deer", 
                "dog", "frog", "horse", "ship", "truck"]


# setup comet
# for notebook (eg google colab). only needed if comet_ml not installed yet.
if COMET_EXPERIMENT and NOTEBOOK:
    pass # if notebook, uncomment:
    # %pip install comet_ml
# after installation for first time, restart notebook!

if COMET_EXPERIMENT:
    # import comet_ml in the top of your file    
    from comet_ml import Experiment
    
    # Add the following code anywhere in your machine learning file
    experiment = Experiment(api_key="EnterYourAPIKey",
                            project_name="exercise4", workspace="irratzo")
                            
# other imports  
import dlipr
import numpy as np
import matplotlib.pyplot as plt
import random as rnd
import tensorflow as tf
from tensorflow import keras
from sklearn.preprocessing import StandardScaler
import os
from pathlib import Path
models = keras.models
layers = keras.layers                            

# some utility logging methods    
def printcomet(string : str = ""):
    # append references (like for "... [1] ..." and so on)
    reflabl = ["[{}]".format(i+1) for i in range(len(REFERENCES))]
    if any([labl in string for labl in reflabl]):
      string += "\nReferences:"
      for i,labl in enumerate(reflabl):
        if labl in string:
          string += "\n{} {}".format(labl, REFERENCES[i])
    
    # print and log
    print(string)
    if COMET_EXPERIMENT:
        experiment.log_text(string)
        
def log_image(filename : str = "unnamed", path : Path = None):
    # the nice thing about vispa is that if the image stays
    # in the src folder, it will be displayed upon execution.
    # the bad thing about that is that it dirties the src folder.
    # alternative: put into a data folder.
    if path:
        path.mkdir(parents=True, exist_ok=True)
        Path(filename).replace(path / filename)
    else:
        path = Path.cwd() # = don't move file
    if COMET_EXPERIMENT:
        experiment.log_image(str(path / filename))

# make output directory ../data/results_taskX.Y_runZ
output_label = "taskX.Y_runZ"
path_src = Path.cwd()
path_data = path_src.parent / "data" / output_label
path_models = path_src.parent / "models" / output_label
printcomet("local output dirs:\n- data: {}\n- models: {}\n- src: {}".format(path_data, path_models, path_src))

printcomet("Constants: COMET_EXPERIMENT {}, NOTEBOOK {}".format(
    COMET_EXPERIMENT, NOTEBOOK))

printcomet("""
# ==============================================================================
# Data Preprocessing
# ==============================================================================
""")

data = dlipr.cifar.load_cifar10()

printcomet("# plot some example images")
filename='examples.png'
dlipr.utils.plot_examples(data, fname=filename)
log_image(filename, path_data)

x_train = data.train_images
y_train = data.train_labels
x_test = data.test_images
y_test = data.test_labels
# y_train_original = y_train.copy()
# y_test_original = y_test.copy()
image_shape = x_train.shape[1:]

def stats(array):
    printcomet("shape {}, type {}".format(array.shape, array.dtype))
    printcomet("(min,max)=({:.2e},{:.2e}), mean={:.2e}, std={:.2e}".format(
        np.amin(array), np.amax(array), np.mean(array), np.std(array)))

# # x_train: first image, first pixel: x_train[0,0,0] == array([59, 62, 63], dtype=uint8)
# #          first image, red channel: x_train[0,:,:,0]
# #          first image, plot:        plt.imshow(x_train[0,:,:,:])
# # value ranges: x pixels in [0,255]
# #               y labels in [0,9]

# preprocess the data in a suitable way
def standardize(*arrays):
    """first array used for mean, std. creates copies.
    """
    first = arrays[0]
    rest = arrays[1:]
    astype = 'float32'

    shape = first.shape
    M = shape[0] # no. of data points
    N = first.size//M # size of one data point
    firs = first.flatten().reshape(M, N) # scaler needs 1D data
    scaler = StandardScaler()
    scaler.fit(firs)
    firs = scaler.transform(firs).reshape(*shape).astype(astype) # CNN needs 2+D data
    
    arrs = [firs,]
    for arr in rest:
        shape = arr.shape
        M = shape[0]
        N = arr.size//M
        arrs.append(scaler.transform(
            arr.flatten().reshape(M,N)).reshape(*shape).astype(astype))

    # # alternative equivalent method, less precise:
    #
    # mean = np.mean(first, axis=0)
    # stdev = np.std(first, axis=0)
    # firs = np.zeros_like(first).astype(astype)
    # for i in range(first.shape[0]):
    #   firs[i] = (first[i] - mean) / stdev
    # arrs = [firs,]
    # for arr in rest:
    #     ar = np.zeros_like(arr).astype(astype)
    #     for i in range(arr.shape[0]):
    #       ar[i] = (arr[i] - mean) / stdev
    #     arrs.append(ar)
    
    return tuple(arrs)

printcomet("standardize image data")
printcomet("x_train stats before standardization:")
stats(x_train)
x_train, x_test = standardize(x_train, x_test)
printcomet("x_train stats after standardization:")
stats(x_train)

# x_train = x_train.astype('float32') / 255
# x_test = x_test.astype('float32') / 255

printcomet("convert label data to one-hot encoding")
y_train = keras.utils.to_categorical(y_train)
y_test = keras.utils.to_categorical(y_test)


printcomet("""
# ==============================================================================
# Define model
# ==============================================================================
""")

kernel_size = (3,3)
activation = 'relu'
model = models.Sequential( # PARTS: 1) CONVPYRAMID 2) FULLYCONNECTED
    [layers.Conv2D(filters=32, # CONVPYRAMID # CONVBLOCK1
                  kernel_size=kernel_size,
                  strides=(1,1), # default
                  padding='same', # = 0-padding, default: 'valid' = no padding
                  input_shape=image_shape),
     layers.BatchNormalization(),                  
     layers.Activation(activation),
     layers.Conv2D(filters=32,
                  kernel_size=kernel_size,
                  padding='same'),
     layers.BatchNormalization(),                  
     layers.Activation(activation),     
     layers.MaxPool2D(pool_size=(2,2), # default
                  strides=None, # default
                  padding='valid'), # default
     layers.Conv2D(filters=64, # CONVBLOCK2
                  kernel_size=kernel_size,
                  padding='same'),
     layers.BatchNormalization(),
     layers.Activation(activation),
     layers.Conv2D(filters=64,
                  kernel_size=kernel_size,
                  padding='same'),
     layers.BatchNormalization(),
     layers.Activation(activation),
     layers.MaxPool2D(),
     layers.Conv2D(filters=128, # CONVBLOCK3
                  kernel_size=kernel_size,
                  padding='same'),
     layers.BatchNormalization(),
     layers.Activation(activation),
     layers.Conv2D(filters=128,
                  kernel_size=kernel_size,
                  padding='same'),
     layers.BatchNormalization(),
     layers.Activation(activation),
     layers.Conv2D(filters=128,
                  kernel_size=kernel_size,
                  padding='same'),
     layers.BatchNormalization(),
     layers.Activation(activation),     
     layers.MaxPool2D(),     
     layers.Flatten(), # FULLYCONNECTED
     layers.Dense(units=128, activation=activation),
     layers.Dropout(rate=0.2),
     layers.Dense(units=10,
                  activation='softmax')
     ])
     
model.compile(optimizer='adam',
             loss='categorical_crossentropy',
             metrics=['accuracy'])
             
model.summary(print_fn=printcomet)

printcomet("""    
# ==============================================================================
# Train
# ==============================================================================
""")

# 'with'-context logs metrics with the prefix 'train_'
with experiment.train():
    model.fit(x_train, y_train,
              batch_size=32,
              epochs=50,
              verbose=2,
              validation_split=0.1,
              callbacks=[
                  keras.callbacks.EarlyStopping( # reference: stackoverflow.com/a/43906235/8116031
                      monitor='val_loss', 
                      min_delta=0, 
                      patience=20, 
                      verbose=1, 
                      mode='auto')])

    # history = model.fit() metrics automatically logged by comet: 
    # "loss", "accuracy", "val_loss", "val_accuracy" now become
    # "train_loss", "train_accuracy", "train_val_loss", "train_val_accuracy"

                    
printcomet("""
# ==============================================================================
# Testing
# ==============================================================================
""")     

# 'with'-context logs metrics with the prefix 'test_'
with experiment.test():
    loss, accuracy = model.evaluate(x_test, y_test)
    printcomet("test_loss {}, test_accuracy {}".format(loss, accuracy))
    metrics = {
      "loss" :loss, # becomes "test_loss"
      "accuracy":accuracy # becomes "test_accuracy"
    }
    experiment.log_metrics(metrics)

printcomet("""
# ==============================================================================
# Postprocessing
# ==============================================================================
""")

def plot_confusion(y_test, y_predicted, class_labels):
    if COMET_EXPERIMENT:
        printcomet("# log confusion matrix using comet's built-in cm; code from [2]")
        
        # this produces a comet error: no upload: no idea why.
        # experiment.log_confusion_matrix(y_true=y_test, y_predicted=yp, labels=class_labels)
        
        # so instead we gonna build the matrix here, then upload that. that works.
        # this 'build matrix' code taken directly from [2].
        n = len(class_labels)
        bins = np.linspace(-0.5, n - 0.5, n + 1)
        C = np.histogram2d(y_test, y_predicted, bins=bins)[0]
        C = C / np.sum(C, axis=0) * 100
        experiment.log_confusion_matrix(matrix=C, labels=class_labels)
    else:
        print("# no comet experiment, so no confusion matrix")
        
# predicted probabilities for the test set
Y_predicted = model.predict(x_test)
y_predicted = np.argmax(Y_predicted, axis=1)

plot_confusion(y_test=data.test_labels, y_predicted=y_predicted, class_labels=CLASS_LABELS) 


printcomet("""
# ==============================================================================
# End
# ==============================================================================
""")

printcomet("save model")
model.save("model.h5")

if COMET_EXPERIMENT:
    experiment.end()
