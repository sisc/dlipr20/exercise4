# ==============================================================================
# exercise4.1.I: CNN for CIFAR10 with >70% accuracy
# ==============================================================================

# Usage:
# none

# ==============================================================================
# Constants, Imports
# ==============================================================================
# constants
COMET_EXPERIMENT = True
NOTEBOOK = False
REFERENCES = [
        "https://git.rwth-aachen.de/3pia/vispa/dlipr", # [1]
        "https://github.com/DavidWalz/dlipr", # [2]
    ]
    
CLASS_LABELS = ["airplane", "automobile", "bird", "cat", "deer", 
                "dog", "frog", "horse", "ship", "truck"]


# setup comet
# for notebook (eg google colab). only needed if comet_ml not installed yet.
if COMET_EXPERIMENT and NOTEBOOK:
    pass # if notebook, uncomment:
    # %pip install comet_ml
# after installation for first time, restart notebook!

if COMET_EXPERIMENT:
    # import comet_ml in the top of your file    
    from comet_ml import Experiment
    
    # Add the following code anywhere in your machine learning file
    experiment = Experiment(api_key="EnterYourAPIKey",
                            project_name="exercise4", workspace="irratzo")
                            
# other imports  
import dlipr
import numpy as np
import matplotlib.pyplot as plt
import random as rnd
import tensorflow as tf
from tensorflow import keras
from sklearn.preprocessing import StandardScaler
import os
from pathlib import Path
models = keras.models
layers = keras.layers                            

# some utility logging methods    
def printcomet(string : str = ""):
    # append references (like for "... [1] ..." and so on)
    reflabl = ["[{}]".format(i+1) for i in range(len(REFERENCES))]
    if any([labl in string for labl in reflabl]):
      string += "\nReferences:"
      for i,labl in enumerate(reflabl):
        if labl in string:
          string += "\n{} {}".format(labl, REFERENCES[i])
    
    # print and log
    print(string)
    if COMET_EXPERIMENT:
        experiment.log_text(string)
        
def log_image(filename : str = "unnamed", path : Path = None):
    # the nice thing about vispa is that if the image stays
    # in the src folder, it will be displayed upon execution.
    # the bad thing about that is that it dirties the src folder.
    # alternative: put into a data folder.
    if path:
        path.mkdir(parents=True, exist_ok=True)
        Path(filename).replace(path / filename)
    else:
        path = Path.cwd() # = don't move file
    if COMET_EXPERIMENT:
        experiment.log_image(str(path / filename))

# make output directory ../data/results_taskX.Y_runZ
output_label = "taskX.Y_runZ"
path_src = Path.cwd()
path_data = path_src.parent / "data" / output_label
path_models = path_src.parent / "models" / output_label
printcomet("local output dirs:\n- data: {}\n- models: {}\n- src: {}".format(path_data, path_models, path_src))

printcomet("Constants: COMET_EXPERIMENT {}, NOTEBOOK {}".format(
    COMET_EXPERIMENT, NOTEBOOK))

printcomet("""
# ==============================================================================
# Data Preprocessing
# ==============================================================================
""")

data = dlipr.cifar.load_cifar10()

printcomet("# plot some example images")
filename='examples.png'
dlipr.utils.plot_examples(data, fname=filename)
log_image(filename, path_data)

x_train = data.train_images
y_train = data.train_labels
x_test = data.test_images
y_test = data.test_labels
# y_train_original = y_train.copy()
# y_test_original = y_test.copy()
image_shape = x_train.shape[1:]

def stats(array):
    printcomet("shape {}, type {}".format(array.shape, array.dtype))
    printcomet("(min,max)=({:.2e},{:.2e}), mean={:.2e}, std={:.2e}".format(
        np.amin(array), np.amax(array), np.mean(array), np.std(array)))

# # x_train: first image, first pixel: x_train[0,0,0] == array([59, 62, 63], dtype=uint8)
# #          first image, red channel: x_train[0,:,:,0]
# #          first image, plot:        plt.imshow(x_train[0,:,:,:])
# # value ranges: x pixels in [0,255]
# #               y labels in [0,9]

# preprocess the data in a suitable way
def standardize(*arrays):
    """first array used for mean, std. creates copies.
    """
    first = arrays[0]
    rest = arrays[1:]
    astype = 'float32'

    shape = first.shape
    M = shape[0] # no. of data points
    N = first.size//M # size of one data point
    firs = first.flatten().reshape(M, N) # scaler needs 1D data
    scaler = StandardScaler()
    scaler.fit(firs)
    firs = scaler.transform(firs).reshape(*shape).astype(astype) # CNN needs 2+D data
    
    arrs = [firs,]
    for arr in rest:
        shape = arr.shape
        M = shape[0]
        N = arr.size//M
        arrs.append(scaler.transform(
            arr.flatten().reshape(M,N)).reshape(*shape).astype(astype))

    # # alternative equivalent method, less precise:
    #
    # mean = np.mean(first, axis=0)
    # stdev = np.std(first, axis=0)
    # firs = np.zeros_like(first).astype(astype)
    # for i in range(first.shape[0]):
    #   firs[i] = (first[i] - mean) / stdev
    # arrs = [firs,]
    # for arr in rest:
    #     ar = np.zeros_like(arr).astype(astype)
    #     for i in range(arr.shape[0]):
    #       ar[i] = (arr[i] - mean) / stdev
    #     arrs.append(ar)
    
    return tuple(arrs)

printcomet("standardize image data")
printcomet("x_train stats before standardization:")
stats(x_train)
x_train, x_test = standardize(x_train, x_test)
printcomet("x_train stats after standardization:")
stats(x_train)

# x_train = x_train.astype('float32') / 255
# x_test = x_test.astype('float32') / 255

printcomet("convert label data to one-hot encoding")
y_train = keras.utils.to_categorical(y_train)
y_test = keras.utils.to_categorical(y_test)


printcomet("""
# ==============================================================================
# Load model
# ==============================================================================
""")



model = keras.models.load_model('model.h5')

                    


printcomet("""
# ==============================================================================
# Postprocessing
# ==============================================================================
""")
        
# predicted probabilities for the test set
Y_predicted = model.predict(x_test)
y_predicted = np.argmax(Y_predicted, axis=1)


printcomet("plot predictions")

def plot_prediction(dir : Path = Path.cwd()):
    printcomet("# plot some predictions using code from [1]")
    
    # collect indices of y_test labels we predicted correctly / wrong
    correct = np.nonzero(y_predicted == data.test_labels)[0]
    wrong = np.nonzero(y_predicted != data.test_labels)[0]
    
    def plot_prediction_grid(filename, y_indices, nrows=3):
        ncols = nrows
        total = nrows * nrows
        plt.figure()
        for i, iy in enumerate(y_indices[:total]):
            plt.subplot(nrows, ncols, i+1)
            plt.imshow(data.test_images[iy], cmap='gray', interpolation='none')
            # plt.imshow(X_test[iy].reshape(28,28), cmap='gray', interpolation='none')
            plt.title("Predicted {}, Class {}".format(y_predicted[iy], data.test_labels[iy]))
        plt.tight_layout()
        plt.savefig(str(dir / filename), bbox_inches='tight')
        if COMET_EXPERIMENT:
            experiment.log_image(str(dir / filename))
        
    plot_prediction_grid("prediction_correct.png", correct)
    plot_prediction_grid("prediction_wrong.png", wrong)
        
plot_prediction(path_data)




printcomet("""
# ==============================================================================
# End
# ==============================================================================
""")

printcomet("save model")
model.save("model.h5")

if COMET_EXPERIMENT:
    experiment.end()
