from comet_ml import Experiment
import numpy as np
import matplotlib.pyplot as plt
import dlipr
from tensorflow import keras
layers = keras.layers


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="EnterYourAPIKey",
                        project_name="exercise4", workspace="EnterGroupWorkspaceHere")

# ----------------------------------------------------------
# Load your model
# ----------------------------------------------------------
model = keras.models.load_model('model.h5')

print(model.summary())
print(model.layers)

# Note: You need to pick the right convolutional layers from your network here
conv1 = model.layers[1]
conv2 = model.layers[3]


# ----------------------------------------------------------
# Plot the convolutional filters in the first layer
# ----------------------------------------------------------


# ---------------------------------------------------------
# Load and preprocess data
# ---------------------------------------------------------
data = dlipr.cifar.load_cifar10()

# prepare the test set the same way as in your training
X_test = ...

i = 12  # choose a good test sample


# ----------------------------------------------------------
# Plot the picture with predictions
# ----------------------------------------------------------


# ----------------------------------------------------------
# Plot activations in convolution layers
# ----------------------------------------------------------
def visualize_activation(A, name='conv'):
    nx, ny, nf = A.shape
    n = np.ceil(nf**.5).astype(int)
    fig, axes = plt.subplots(n, n, figsize=(5, 5))
    fig.subplots_adjust(left=0.05, bottom=0.05, right=0.95, top=0.95, hspace=0, wspace=0)
    for i in range(n**2):
        ax = axes.flat[i]
        if i < nf:
            ax.imshow(A[..., i], origin='upper', cmap=plt.cm.Greys)
            ax.xaxis.set_visible(False)
            ax.yaxis.set_visible(False)
        else:
            ax.axis('Off')
    fig.suptitle(name, va='bottom')
    fig.savefig('%s.png' % name, bbox_inches='tight')
    return fig


conv_layers = [l for l in model.layers if type(l) == layers.Conv2D]

for j, conv in enumerate(conv_layers):

    conv_model = keras.models.Model(model.inputs, [conv.output])
    # plot the activations for test sample i
    Xin = X_test[i][np.newaxis]
    Xout1 = conv_model.predict(Xin)[0]
    fig = visualize_activation(Xout1, 'image%i-conv%i' % (i, j))
    experiment.log_figure(figure=fig)
