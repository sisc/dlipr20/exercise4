#Correction
##Training/Evaluation
###Task 1 
9/9 P - Well done
###Task 2 
2/2 P 
###Task 3 
3/3 P
##Task Visualization
###Task 1
2/2 P
###Task 2
4/4 P
#Total 20/20 P As always: Very nice :)


**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise 4 Convolutional Neural Networks**

Date: 18.05.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |



**Table of Contents**

- [Preliminary remarks](#preliminary-remarks)
    - [Experiment tagging](#experiment-tagging)
- [ConvNet: Training and evaluation [14 P]](#convnet-training-and-evaluation-14-p)
    - [I. Set up and train a convolutional network on the CIFAR-10 classification task. [9 P]](#i-set-up-and-train-a-convolutional-network-on-the-cifar-10-classification-task-9-p)
        - [Data preprocessing](#data-preprocessing)
            - [Looking at the input data](#looking-at-the-input-data)
            - [Preprocessing of the image data](#preprocessing-of-the-image-data)
            - [Preprocessing of the label data](#preprocessing-of-the-label-data)
        - [CNN best practices from the lecture](#cnn-best-practices-from-the-lecture)
            - [Input data](#input-data)
            - [CNN architecture](#cnn-architecture)
            - [Convolutional blocks](#convolutional-blocks)
            - [Fully connected network](#fully-connected-network)
            - [Optimization](#optimization)
        - [Our CNN model architecture and results](#our-cnn-model-architecture-and-results)
    - [II. Plot the confusion matrix. What do you observe? [2 P]](#ii-plot-the-confusion-matrix-what-do-you-observe-2-p)
    - [III. Plot a number of falsely classified images along with the predicted class scores. What kinds of misclassification do you observe, why do they occur? [3 P]](#iii-plot-a-number-of-falsely-classified-images-along-with-the-predicted-class-scores-what-kinds-of-misclassification-do-you-observe-why-do-they-occur-3-p)
- [ConvNet: Visualization [6 P]](#convnet-visualization-6-p)
    - [I. Plots the weights of you in the first layer of your network and see if you can make any sens of it. [2 P]](#i-plots-the-weights-of-you-in-the-first-layer-of-your-network-and-see-if-you-can-make-any-sens-of-it-2-p)
    - [II. Visualize the activations in the first 2 layers of your network, for two input images of your choice and describe what you see. Does it meet your expectations? [4 P]](#ii-visualize-the-activations-in-the-first-2-layers-of-your-network-for-two-input-images-of-your-choice-and-describe-what-you-see-does-it-meet-your-expectations-4-p)
- [References](#references)




# Preliminary remarks #

## Experiment tagging ##

Final experiments in this project (only one task, task1) are tagged with the tag `FINAL`, the respective task tag, e.g. `taskX.Y.Z`, and optionally more tags to better distinguish them. Experiments tagged with `dev`, should all be hidden away in the [archive](https://www.comet.ml/irratzo/exercise4/archive). They are development stage and **not** final. Specific experiments are referred to in the notes via their tags and/or hash or hyperlink. 


# ConvNet: Training and evaluation [14 P] #

  * 

## I. Set up and train a convolutional network on the CIFAR-10 classification task. [9 P] ##

> Think about data preprocessing: how can we improve the training?

> Tweak, tune and train your network to at least 70% test accuracy. Plot the trainings curves.

> Note: it doesn’t have to be fancy yet. We’ll train advanced architectures later. 

> Describe and motivate your network design. Hand in the relevant code / plots!
    
### Data preprocessing ###

#### Looking at the input data ####

The shapes of the original data:

`original data shapes: x_train (50000, 32, 32, 3) y_train (50000, 1) x_test (10000, 32, 32, 3) y_test (10000, 1)`

So for example, the first pixel of the first image in `x_train` has three values for the RGB channels:

```python
>>> x_train[0,0,0]
array([59, 62, 63], dtype=uint8)
```

And the red channel of the first image looks like this:

```python
>>> x_train[0,:,:,0]
array([[ 59,  43,  50, ..., 158, 152, 148],
       [ 16,   0,  18, ..., 123, 119, 122],
       [ 25,  16,  49, ..., 118, 120, 109],
       ...,
       [208, 201, 198, ..., 160,  56,  53],
       [180, 173, 186, ..., 184,  97,  83],
       [177, 168, 179, ..., 216, 151, 123]], dtype=uint8)
```

The pixel value range is (0,255).

```python
>>> np.amin(x_train)
0
>>> np.amax(x_train)
255
```

`y_train` looks like this. The ten labels mean: airplane, automobile, bird, cat,
deer, dog, frog, horse, ship, truck.

```python
>>> y_train
array([[6],
       [9],
       [9],
       ...,
       [9],
       [1],
       [1]], dtype=uint8)
>>> np.amin(y_train)
0
>>> np.amax(y_train)
9
```

The first image in the dataset is one of a frog = category 6.

```python
plt.imshow(x_train[0,:,:,:])
```

![](https://www.comet.ml/api/image/notes/download?imageId=UF9dDSWJ12w5uykuBUBGTdPyH&objectId=51eef9b9d40d44fda9d17fae64cb080e)

#### Preprocessing of the image data ####

Firstly, TensorFlow requires input data to be in 32 bit.

```python
dataset = dataset.astype('float32')
```

Furthermore, we have to think about data scaling. This ensures that input data with different scales do not produce overly large or small weights, which would impact the training negatively. Two common data scaling techniques are normalization and standardization. **Normalization**: transform input data such that all values lie within `[0,1]`. **Standardization**: the data lies in `[-1,1]`, its mean value is `0`, and its standard deviation is `1`. Achieved by subtracting the mean pixel value and dividing the result by the standard deviation of the pixel values [2].

Stanford class CS231n Convolutional neural networks recommends [3]:

> The recommended preprocessing is to center the data to have mean of zero, and normalize its scale to [-1, 1] along each feature.

The standardization has to be performed relative to the training data [3]:

> An important point to make about the preprocessing is that any preprocessing statistics (e.g. the data mean) must only be computed on the training data, and then applied to the validation / test data.

Another reason to employ normalization and standardization by default as a good practice is that, even though our machine learning model may not be require one or the other by design (CNN), or even though our data does not follow a normal=Gaussian distribution, it improves comparability for results from different models and datasets [4]. 

For standardization, we wrote the function `standardize`. It uses `scikit-learn` which provides this out of the box.

```python
from sklearn.preprocessing import StandardScaler
# ...

def standardize(*arrays):
    """first array used for mean, std. creates copies.
    """
    first = arrays[0]
    rest = arrays[1:]
    astype = 'float32'

    shape = first.shape
    M = shape[0] # no. of data points
    N = first.size//M # size of one data point
    firs = first.flatten().reshape(M, N) # scaler needs 1D data
    scaler = StandardScaler()
    scaler.fit(firs)
    firs = scaler.transform(firs).reshape(*shape).astype(astype) # CNN needs 2+D data
    
    arrs = [firs,]
    for arr in rest:
        shape = arr.shape
        M = shape[0]
        N = arr.size//M
        arrs.append(scaler.transform(
            arr.flatten().reshape(M,N)).reshape(*shape).astype(astype))

    # # alternative equivalent method, less precise:
    #
    # mean = np.mean(first, axis=0)
    # stdev = np.std(first, axis=0)
    # firs = np.zeros_like(first).astype(astype)
    # for i in range(first.shape[0]):
    #   firs[i] = (first[i] - mean) / stdev
    # arrs = [firs,]
    # for arr in rest:
    #     ar = np.zeros_like(arr).astype(astype)
    #     for i in range(arr.shape[0]):
    #       ar[i] = (arr[i] - mean) / stdev
    #     arrs.append(ar)
    
    return tuple(arrs)
```

The effect is as desired.

```python
>>> stats(x_train)
shape (50000, 32, 32, 3), type uint8, (min,max)=(0.00e+00,2.55e+02), mean=1.21e+02, std=6.42e+01
>>> x_train, x_test = standardize(x_train, x_test)
>>> stats(x_train)
shape (50000, 32, 32, 3), type float32, (min,max)=(-2.21e+00,2.63e+00), mean=2.71e-10, std=1.00e+00
```

Another form of image data preprocessing for CNNs, which we will not emply here, is data augmentation. This is a technique that enlarges a dataset by producing varying copies from existing images, for example flipping them, changes their saturation etc. [5] [6]. 


#### Preprocessing of the label data ####

Our label data has to be in one-hot encoding format, i.e. shape `(50000)` to `(50000, 10)` for `y_train`.

```python
y_train = keras.utils.to_categorical(y_train)
y_test = keras.utils.to_categorical(y_test)
```

### CNN best practices from the lecture ###

These are the best practices that lecture Dr. Glombitza mentioned in the lecture [1]. *Addenda* points are extensions, added here by us, to the original quotes from the lecture.

#### Input data ####

- for CNN to work, require some spatial correlation like translational invariance, symmetries in input data.

#### CNN architecture ####

- p.40 **summary**: use ADAM as optimizer, use 3x3 filters, use ReLu activation, use not only convolution, but after a few convolutional layers, add pooling. after pooling, increase no. of filters, and after say e.g. four to six conv. layers, add pooling, and then a fully connected network or layer, before softmax output.  
- p.37: start with a smaller model, then increase as needed, not the other way round, cause CNN very memory-bound.
- p.31: first part of the complete model (the CNN) is also called **convolutional pyramid (CP)** (sometimes confusingly also called CNN, just as the whole model). Its task is to transform WxHx3 input (width, height, 3 color channels) into 1x1xC output (vector with depth=length of number of classes=labels=categories), all the while transforming low-level into high-level features through convolution. Second part of the model is the **fully connected network (FCN)**, also called '**trainable classifier**' which then learns these high-level abstract features. In other words: some conv. layers, a pooling layer, increase no. of filters for next conv. layers, conv., conv. ... pooling again, conv., conv ... then can use global pooling or flatten operation to make a vector; then use a FCN, meaning the standard `Dense` layout from Keras. Example see p.39. When someone says 'convolutional block', this usually means filter=convolution plus pooling layer. Normally, pyramid consists of 1-5 conv. blocks.
- p.34: **VGGNet** example architecture: you can design your first CNN very similarly. For a conv. layer, dimension is WxHxD, with D=depth=no. of filters. The last activation is always softmax.
- **VGGNet** *Addendum*: VGGNet has been superseded by more advanced architectures like ResNet [8]. 

#### Convolutional blocks ####

- **filters**: only odd sizes to center filter. typical filter size is 3x3. At the moment in the literature, ca. 99% of the models are using 3x3 filters (reason this works: stacking conv. layers increases receptive field to capture larger/more complex features).   
- p.25 **padding**: to increase depth of convolutional pyramid - without it, each filter would decrease image size.
- p.26 **stride**: to reduce image dimensions, save RAM, computational effort.
- p.27 **dilation** / atrous convolution: scatters/distributes filter over a wider area of pixels, not all of which get considered anymore. not very often used. but may be used if in need of very large field of vision, e.g. for semantic image segmentation.
- p.28 **pooling**:
    - there is **max pooling** and overlapping pooling. the latter is not widely used.
    - max pooling makes network more invariant to small perturbations, more often shows better performance.
- p.29 **global pooling**: kind of a regularizer for conv. block, also used for visualization.

#### Fully connected network ####

- poll: apply **dropout** only between FC layers. Reason: dropout as a regularization technique fights overfitting, but overfitting is normally not a problem in the CP: Convolution reduces parameter count, as opposed to dense layers. 
- **dropout** *Addendum*: reference [7] explains why dropout layers should not be used in the CP. Instead, it advises to "Insert a **batch normalization** layer between convolution and activation layers." Thus making batch normalization the 'dropout' of the convolutional blocks. 

#### Optimization ####

- prefer ADAM in most cases (adaptive moment estimation), most-used optimizer in DL.
- “Friends don’t let friends use **minibatches** larger than 32” - Yann LeCun

### Our CNN model architecture and results ###

We compiled all models with ADAM as optimizer with default values, categorical crossentropy. We trained all models with batch size 32, epochs up to ca. 100. Throughout the CP, we used the recommended filter size `kernel_size = (3,3)`, and `activation = 'relu'`. We logged the train, validation and test accuracy and loss (for test results, see metrics in respective comet experiment).

On of our first attempts was, with bearing all of the above best practices in mind, with an architecture with two convolutional blocks with (32, 64) filters in the CP (convolutional pyramid), and a FCN (fully connected network) with only one softmax output layer. 

This is experiment [f5e6b](https://www.comet.ml/irratzo/exercise4/f5e6b7d5801b403cb9eea4305f102da5?experiment-tab=metrics).

**This experiment's model achieved 70.4% test accuracy.**

```python
kernel_size = (3,3)
activation = 'relu'
model = models.Sequential(
    [layers.Conv2D(filters=32,
                  kernel_size=kernel_size,
                  strides=(1,1), # default
                  padding='valid', # default
                  activation=activation,
                  input_shape=image_shape),
     layers.MaxPool2D(pool_size=(2,2), # default
                  strides=None, # default
                  padding='valid'), # default
     layers.Conv2D(filters=64,
                  kernel_size=kernel_size,
                  activation=activation),
     layers.MaxPool2D(),
     layers.Conv2D(filters=64,
                  kernel_size=kernel_size,
                  activation=activation),
     layers.MaxPool2D(),     
     layers.Flatten(),
     layers.Dense(units=10,
                  activation='softmax')
     ])
     
model.compile(optimizer='adam',
             loss='categorical_crossentropy',
             metrics=['accuracy'])
```

```python
>>> model.summary(print_fn=printcomet)
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #
=================================================================
conv2d (Conv2D)              (None, 30, 30, 32)        896
_________________________________________________________________

max_pooling2d (MaxPooling2D) (None, 15, 15, 32)        0
_________________________________________________________________
conv2d_1 (Conv2D)            (None, 13, 13, 64)        18496
_________________________________________________________________

max_pooling2d_1 (MaxPooling2 (None, 6, 6, 64)          0
_________________________________________________________________

conv2d_2 (Conv2D)            (None, 4, 4, 64)          36928
_________________________________________________________________

max_pooling2d_2 (MaxPooling2 (None, 2, 2, 64)          0
_________________________________________________________________
flatten (Flatten)            (None, 256)               0
_________________________________________________________________

dense (Dense)                (None, 10)                2570
=================================================================
Total params: 58,890

Trainable params: 58,890

Non-trainable params: 0
```

We learned earlier on that we had to use EarlyStopping because the validation loss tended to increase quite early in the training phase, more often than not. For optimizing the model, we orientied ourselves on the VGGNet architecture from the lecture. In the CP, we added more convolution layers, and added batch normalization as a kind of regularizer, as motivated in [7]. We added `padding=same` to all `Conv2D` layers (i.e., do zero-padding) and let default `padding=value` (i.e., no padding) in the pooling layers. In the FCN, we added a dense layer with as many units as the filters in the last convolution block before flattening, so the FCN can learn these abstract features.

This is experiment [f7946](https://www.comet.ml/irratzo/exercise4/f79464494bba456480f3a5a95633c9d7?experiment-tab=metrics).

**This experiment's model achieved 83.9% test accuracy.**

**This is our** `FINAL` **experiment.**

```python
kernel_size = (3,3)
activation = 'relu'
model = models.Sequential( # PARTS: 1) CONVPYRAMID 2) FULLYCONNECTED
    [layers.Conv2D(filters=32, # CONVPYRAMID # CONVBLOCK1
                  kernel_size=kernel_size,
                  strides=(1,1), # default
                  padding='same', # = 0-padding, default: 'valid' = no padding
                  input_shape=image_shape),
     layers.BatchNormalization(),                  
     layers.Activation(activation),
     layers.Conv2D(filters=32,
                  kernel_size=kernel_size,
                  padding='same'),
     layers.BatchNormalization(),                  
     layers.Activation(activation),     
     layers.MaxPool2D(pool_size=(2,2), # default
                  strides=None, # default
                  padding='valid'), # default
     layers.Conv2D(filters=64, # CONVBLOCK2
                  kernel_size=kernel_size,
                  padding='same'),
     layers.BatchNormalization(),
     layers.Activation(activation),
     layers.Conv2D(filters=64,
                  kernel_size=kernel_size,
                  padding='same'),
     layers.BatchNormalization(),
     layers.Activation(activation),
     layers.MaxPool2D(),
     layers.Conv2D(filters=128, # CONVBLOCK3
                  kernel_size=kernel_size,
                  padding='same'),
     layers.BatchNormalization(),
     layers.Activation(activation),
     layers.Conv2D(filters=128,
                  kernel_size=kernel_size,
                  padding='same'),
     layers.BatchNormalization(),
     layers.Activation(activation),
     layers.Conv2D(filters=128,
                  kernel_size=kernel_size,
                  padding='same'),
     layers.BatchNormalization(),
     layers.Activation(activation),     
     layers.MaxPool2D(),     
     layers.Flatten(), # FULLYCONNECTED
     layers.Dense(units=128, activation=activation),
     layers.Dropout(rate=0.2),
     layers.Dense(units=10,
                  activation='softmax')
     ])
```

```python
>>> model.summary(print_fn=printcomet)
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
conv2d (Conv2D)              (None, 32, 32, 32)        896       
_________________________________________________________________
batch_normalization (BatchNo (None, 32, 32, 32)        128       
_________________________________________________________________
activation (Activation)      (None, 32, 32, 32)        0         
_________________________________________________________________
conv2d_1 (Conv2D)            (None, 32, 32, 32)        9248      
_________________________________________________________________
batch_normalization_1 (Batch (None, 32, 32, 32)        128       
_________________________________________________________________
activation_1 (Activation)    (None, 32, 32, 32)        0         
_________________________________________________________________
max_pooling2d (MaxPooling2D) (None, 16, 16, 32)        0         
_________________________________________________________________
conv2d_2 (Conv2D)            (None, 16, 16, 64)        18496     
_________________________________________________________________
batch_normalization_2 (Batch (None, 16, 16, 64)        256       
_________________________________________________________________
activation_2 (Activation)    (None, 16, 16, 64)        0         
_________________________________________________________________
conv2d_3 (Conv2D)            (None, 16, 16, 64)        36928     
_________________________________________________________________
batch_normalization_3 (Batch (None, 16, 16, 64)        256       
_________________________________________________________________
activation_3 (Activation)    (None, 16, 16, 64)        0         
_________________________________________________________________
max_pooling2d_1 (MaxPooling2 (None, 8, 8, 64)          0         
_________________________________________________________________
conv2d_4 (Conv2D)            (None, 8, 8, 128)         73856     
_________________________________________________________________
batch_normalization_4 (Batch (None, 8, 8, 128)         512       
_________________________________________________________________
activation_4 (Activation)    (None, 8, 8, 128)         0         
_________________________________________________________________
conv2d_5 (Conv2D)            (None, 8, 8, 128)         147584    
_________________________________________________________________
batch_normalization_5 (Batch (None, 8, 8, 128)         512       
_________________________________________________________________
activation_5 (Activation)    (None, 8, 8, 128)         0         
_________________________________________________________________
conv2d_6 (Conv2D)            (None, 8, 8, 128)         147584    
_________________________________________________________________
batch_normalization_6 (Batch (None, 8, 8, 128)         512       
_________________________________________________________________
activation_6 (Activation)    (None, 8, 8, 128)         0         
_________________________________________________________________
max_pooling2d_2 (MaxPooling2 (None, 4, 4, 128)         0         
_________________________________________________________________
flatten (Flatten)            (None, 2048)              0         
_________________________________________________________________
dense (Dense)                (None, 128)               262272    
_________________________________________________________________
dropout (Dropout)            (None, 128)               0         
_________________________________________________________________
dense_1 (Dense)              (None, 10)                1290      
=================================================================
Total params: 700,458
Trainable params: 699,306
Non-trainable params: 1,152
```

The following plots are for the `task1.I`-tagged experiments f5e6b through f7946 (see above), representing the iteration process of our model development.

Plot: `train_accuracy` vs `step`

![](https://www.comet.ml/api/image/notes/download?imageId=jQ8veP7x5ZOoUQrtPnqiPhLET&objectId=51eef9b9d40d44fda9d17fae64cb080e)

Plot: `train_loss` vs `step`

![](https://www.comet.ml/api/image/notes/download?imageId=7yp1b7aTyQdH4J26CHzMWAKTl&objectId=51eef9b9d40d44fda9d17fae64cb080e)

Plot: `train_val_accuracy` vs `step`

![](https://www.comet.ml/api/image/notes/download?imageId=PKCMjtDWMck1jG0N1jMAcTCW1&objectId=51eef9b9d40d44fda9d17fae64cb080e)

Plot: `train_val_loss` vs `step`

![](https://www.comet.ml/api/image/notes/download?imageId=AzmGo1LHJHrrcEtzEmF9aHlRk&objectId=51eef9b9d40d44fda9d17fae64cb080e)



## II. Plot the confusion matrix. What do you observe? [2 P] ##

The following figure shows the confiusion matrix for the experiment with the tag  `final`.
The predictions are all above 75%, but their is a trend for the network to have an uncertainty when categorizing bird and cat images.
Bird images get often confused with cat images (6.91 %) and cat images with dog images (13.73%). This reflects in an overall correct categorization of 75%
for birds and 76% for cats.

The used network classifies the other categories more often correctly with a prediction from 80 to 90 percent.
![confusionMatrix](https://www.comet.ml/api/image/notes/download?imageId=pfWQEG8Ofg3vmlLMZnOuMOpR5&objectId=51eef9b9d40d44fda9d17fae64cb080e)
 

## III. Plot a number of falsely classified images along with the predicted class scores. What kinds of misclassification do you observe, why do they occur? [3 P] ##

The following figures show some misspredicted images.
It seems that the network has problems to differentiate living things or animals. On hypothese could be that natural forms are harder to put into categories than 
geometric forms with sharper edges. 

 ![](https://www.comet.ml/api/image/notes/download?imageId=IXvziql6ZC86AlGHipsaSQDI3&objectId=51eef9b9d40d44fda9d17fae64cb080e)

![](https://www.comet.ml/api/image/notes/download?imageId=pzzXOZTSkNaXinfiBSXES7PNG&objectId=51eef9b9d40d44fda9d17fae64cb080e)

![](https://www.comet.ml/api/image/notes/download?imageId=hXw51cwy5amSmT4oh9WdEzH6Z&objectId=51eef9b9d40d44fda9d17fae64cb080e)

![](https://www.comet.ml/api/image/notes/download?imageId=KKCDcdMJIJHjtLdp8OL5cBR4y&objectId=51eef9b9d40d44fda9d17fae64cb080e)

Another approach:

Code:
```python
# predicted probabilities for the test set
Y_predicted = model.predict(x_test)
y_predicted = np.argmax(Y_predicted, axis=1)


printcomet("plot predictions")

def plot_prediction(dir : Path = Path.cwd()):
    printcomet("# plot some predictions using code from [1]")
    
    # collect indices of y_test labels we predicted correctly / wrong
    correct = np.nonzero(y_predicted == data.test_labels)[0]
    wrong = np.nonzero(y_predicted != data.test_labels)[0]
    
    def plot_prediction_grid(filename, y_indices, nrows=3):
        ncols = nrows
        total = nrows * nrows
        plt.figure()
        for i, iy in enumerate(y_indices[:total]):
            plt.subplot(nrows, ncols, i+1)
            plt.imshow(data.test_images[iy], cmap='gray', interpolation='none')
            # plt.imshow(X_test[iy].reshape(28,28), cmap='gray', interpolation='none')
            plt.title("Predicted {}, Class {}".format(y_predicted[iy], data.test_labels[iy]))
        plt.tight_layout()
        plt.savefig(str(dir / filename), bbox_inches='tight')
        if COMET_EXPERIMENT:
            experiment.log_image(str(dir / filename))
        
    plot_prediction_grid("prediction_correct.png", correct)
    plot_prediction_grid("prediction_wrong.png", wrong)
        
plot_prediction(path_data)
```

`class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']`

Plot: correct predictions
![](https://www.comet.ml/api/image/notes/download?imageId=LK0UZNUBQp0tTjcl9jfG3L7iV&objectId=51eef9b9d40d44fda9d17fae64cb080e)

Plot: wrong predictions
![](https://www.comet.ml/api/image/notes/download?imageId=fOJTO0G8hgl9rrtrvtlaBRC2y&objectId=51eef9b9d40d44fda9d17fae64cb080e)
 
The deer bottom center, predicted as airplane: the background is a blue sky, so it seems the network has learned to connect blue skies with airplanes, i.e. learned their common background, rather than the airplane.

The frog, top center, predicted as dog: the frog is brown, and bulky, so the network seems to have learned to identify these features with dogs.

The ship, top left, predicted as airplane: the perspective makes the ship's nose look like that of an airplane. And again, the blue sky.


# ConvNet: Visualization [6 P] #



## I. Plots the weights of you in the first layer of your network and see if you can make any sens of it. [2 P] ##

The weights are stored in an array with shape (3,3,3,32). The first and second index represents the kernel size (3,3), the third index gives the color separation between red, green and blue and the fourth index gives the amount of filters.
In the following plot one can see the weights. Each 3x3 picture represents one kernel with size (3,3). Each colum represents one of the three color channels. Each row represents one filter. 
One can see that the first filter can identify horizontal edges in all color channels, while it additionally takes diagonal shapes from left top to right bottom in the first color channel and from left bottom to top right in the second color channel.
The second filter weights the upper left and lower right high, the third the middle left and the lower right and so on. 

 ![](https://www.comet.ml/api/image/notes/download?imageId=z92UlCi7TKsaWQ02megHFEyWC&objectId=51eef9b9d40d44fda9d17fae64cb080e)

## II. Visualize the activations in the first 2 layers of your network, for two input images of your choice and describe what you see. Does it meet your expectations? [4 P] ##

These two input images are used:

![](https://www.comet.ml/api/image/notes/download?imageId=3MGpvFhy1HEp83cg29LrhU8ye&objectId=51eef9b9d40d44fda9d17fae64cb080e)

![](https://www.comet.ml/api/image/notes/download?imageId=c7Y9hRxX5y4z821C6yPLTyDWX&objectId=51eef9b9d40d44fda9d17fae64cb080e)

In the first convolutional layer the following activations are used:

![](https://www.comet.ml/api/image/notes/download?imageId=u3CrxpSCwvBCFV9ELclb0A2nb&objectId=51eef9b9d40d44fda9d17fae64cb080e)

![](https://www.comet.ml/api/image/notes/download?imageId=Fvhj2RohtOCkWRRl0kdDm7h2p&objectId=51eef9b9d40d44fda9d17fae64cb080e)

As expected, the network filters out the shape of the object on the picture, but the activations look not that different yet.
The activations make the picture easier to classify for humans too, since they recognise shapes that are only faint differences in color. For example, the head of the dog is visible in the activations but faint in the original picture.

The second layer uses these activations:

![](https://www.comet.ml/api/image/notes/download?imageId=HHFh3EUcrXvvAblY1UTmBY32O&objectId=51eef9b9d40d44fda9d17fae64cb080e)

![](https://www.comet.ml/api/image/notes/download?imageId=iE2TaT5bQF2MT0IbKtww32Oao&objectId=51eef9b9d40d44fda9d17fae64cb080e)

Here the activations differ more, but one can still see the shapes of the objects on the picture.
On the visualization of the activation of the third convolutional layer, the pictures are much harder to fit to the object, here the activations specialize on different structures in the picture. The activations of the third layer look visualized:

![](https://www.comet.ml/api/image/notes/download?imageId=K1xq2vJtZi9pk3tN3Ov9Q0FUb&objectId=51eef9b9d40d44fda9d17fae64cb080e) 

![](https://www.comet.ml/api/image/notes/download?imageId=ukTySpcjo8wivLimfew9e55Fw&objectId=51eef9b9d40d44fda9d17fae64cb080e)



# References #

- [1] RWTH DLiPR SS20 Part 4: Convolutional Neural Networks. Lecture Slides. 2020.
- [2] machinelearningmastery.com. How to use Data Scaling Improve Deep Learning Model Stability and Performance. August 6, 2019. URL: https://machinelearningmastery.com/how-to-improve-neural-network-stability-and-modeling-performance-with-data-scaling/ 
- [3] CS231n Convolutional Neural Networks for Visual Recognition. Spring 2020. Course Website. Module 1: Neural Networks. Neural Networks Part 2: Setting up the Data and the Loss. URL: https://cs231n.github.io/neural-networks-2/
- [4] analyticsvidhya.com. Feature Scaling for Machine Learning: Understanding the Difference Between Normalization vs. Standardization. April 3, 2020. URL: https://www.analyticsvidhya.com/blog/2020/04/feature-scaling-machine-learning-normalization-standardization/
- [5] TensorFlow Tutorials. Data augmentation. Retrieved: 17.05.2020. URL: https://www.tensorflow.org/tutorials/images/data_augmentation
- [6] nanonets.com. Data Augmentation | How to use Deep Learning when you have Limited Data — Part 2. 2019. URL: https://nanonets.com/blog/data-augmentation-how-to-use-deep-learning-when-you-have-limited-data-part-2/
- [7] kdnuggets.com. Don’t Use Dropout in Convolutional Networks. September 2018. URL: https://www.kdnuggets.com/2018/09/dropout-convolutional-networks.html
- [8] stackexchange.com. How should I standardize input when fine-tuning a CNN? Answer by DeltaIV. January 22, 2019. URL: https://stats.stackexchange.com/a/388461/176347














